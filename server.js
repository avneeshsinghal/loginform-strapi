var express = require('express');
var app = express();
var path = require('path');

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/loginform.html'));
  
});

app.use('/public', express.static(path.join(__dirname, '/AdminLTE-2.4.5')));

 app.get('/loginform.js', function(req, res) {
    res.sendFile(path.join(__dirname + '/loginform.js'));
});

 app.get('/index2.html', function(req, res) {
    res.sendFile(path.join(__dirname + '/AdminLTE-2.4.5/index2.html'));
});

app.listen(3000);